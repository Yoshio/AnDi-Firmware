#MSG_IMPROVE_BED_OFFSET_AND_SKEW_LINE2 c=14 r=0
" of 4"
" van 4"

#MSG_MEASURE_BED_REFERENCE_HEIGHT_LINE2 c=14 r=0
" of 9"
" van 9"

#MSG_MEASURED_OFFSET c=0 r=0
"[0;0] point offset"
"[0;0] punt offset"

#MSG_CRASH_DET_ONLY_IN_NORMAL c=20 r=4
"\x1b[2JCrash detection can\x1b[1;0Hbe turned on only in\x1b[2;0HNormal stand"
"\x1b[2JCrashdetectie kan\x1b[1;0Halleen in stand normaal\x1b[2;0Hgebruikt worden"

#MSG_CRASH_DET_STEALTH_FORCE_OFF c=20 r=4
"\x1b[2JWARNING:\x1b[1;0HCrash detection\x1b[2;0Hdisabled in\x1b[3;0HStealth stand"
"\x1b[2JWARNING:\x1b[1;0HCrashdetectie\x1b[2;0Huitgeschakeld in\x1b[3;0HStealth stand"

#
">Cancel"
">Annuleren"

#MSG_BABYSTEPPING_Z c=20 r=0
"Adjusting Z"
"Z is ingesteld"

#MSG_SELFTEST_CHECK_ALLCORRECT c=20 r=0
"All correct      "
"Allemaal goed    "

#MSG_WIZARD_DONE c=20 r=8
"All is done. Happy printing!"
"Klaar. Happy printing!"

#
"Ambient"
"EINSY  "

#MSG_PRESS c=20 r=0
"and press the knob"
"en druk op knop"

#MSG_CONFIRM_CARRIAGE_AT_THE_TOP c=20 r=2
"Are left and right Z~carriages all up?"
"Zijn beide Z wagen heelemaal boven?"

#MSG_AUTO_DEPLETE_ON c=17 r=1
"SpoolJoin    [on]"
"SpoolJoin   [aan]"

#
"SpoolJoin   [N/A]"
"SpoolJoin   [N/V]"

# MSG_AUTO_DEPLETE_OFF c=17 r=1
"SpoolJoin   [off]"
"SpoolJoin   [uit]"

#MSG_AUTO_HOME c=0 r=0
"Auto home"
"Startpositie"

#MSG_AUTOLOAD_FILAMENT c=17 r=0
"AutoLoad filament"
"Auto-Laden Filament"

#MSG_AUTOLOADING_ONLY_IF_FSENS_ON c=20 r=4
"Autoloading filament available only when filament sensor is turned on..."
"Autoloading filament alleen beschikbaar wanneer filamentsensor is ingeschakeld ..."

#MSG_AUTOLOADING_ENABLED c=20 r=4
"Autoloading filament is active, just press the knob and insert filament..."
"Automatisch laden van flament is actief, druk de knop en laad filament..."

#MSG_SELFTEST_AXIS_LENGTH c=0 r=0
"Axis length"
"Aslengte"

#MSG_SELFTEST_AXIS c=0 r=0
"Axis"
"As"

#MSG_SELFTEST_BEDHEATER c=0 r=0
"Bed / Heater"
""

#MSG_BED_DONE c=0 r=0
"Bed done"
""

#MSG_BED_HEATING c=0 r=0
"Bed Heating"
""

#MSG_BED_CORRECTION_MENU c=0 r=0
"Bed level correct"
""

#MSG_BED_LEVELING_FAILED_POINT_LOW c=20 r=4
"Bed leveling failed. Sensor didnt trigger. Debris on nozzle? Waiting for reset."
"Bed leveling mislukt. Sensor heeft niet geactiveerd. Puin op tuit? Wacht op reset."

#MSG_BED_LEVELING_FAILED_PROBE_DISCONNECTED c=20 r=4
"Bed leveling failed. Sensor disconnected or cable broken. Waiting for reset."
"Bed leveling mislukt. Sensor losgekoppeld of kabel defect. Wacht op reset."

#MSG_BED_LEVELING_FAILED_POINT_HIGH c=20 r=4
"Bed leveling failed. Sensor triggered too high. Waiting for reset."
"Bed leveling mislukt. Sensor te hoog geactiveerd. Wacht op reset."

#MSG_BED c=0 r=0
"Bed"
""

#MSG_MENU_BELT_STATUS c=15 r=1
"Belt status"
"Riem status"

#MSG_RECOVER_PRINT c=20 r=2
"Blackout occurred. Recover print?"
"Stroomstoring. Print herstellen?"

#
"Calibrating home"
"Kalibreren start"

#MSG_CALIBRATE_BED c=0 r=0
"Calibrate XYZ"
"Kalibratie XYZ"

#MSG_HOMEYZ c=0 r=0
"Calibrate Z"
"Kalibratie Z"

#MSG_CALIBRATE_PINDA c=17 r=1
"Calibrate"
"Kalibreren"

#MSG_MOVE_CARRIAGE_TO_THE_TOP c=20 r=8
"Calibrating XYZ. Rotate the knob to move the Z carriage up to the end stoppers. Click when done."
"Kalibreren van XYZ. Draai de knop om de Z-wagen omhoog te gaan tot het einde stoppers. Druk knop als klaar."

#MSG_CALIBRATE_Z_AUTO c=20 r=2
"Calibrating Z"
"Kalibrere Z"

#MSG_MOVE_CARRIAGE_TO_THE_TOP_Z c=20 r=8
"Calibrating Z. Rotate the knob to move the Z carriage up to the end stoppers. Click when done."
"Kalibreren van Z. Draai de knop om de Z-wagen omhoog te gaan tot het einde stoppers. Druk knop als klaar."

#MSG_HOMEYZ_DONE c=0 r=0
"Calibration done"
"Kalibratie klaar"

#MSG_MENU_CALIBRATION c=0 r=0
"Calibration"
"Kalibratie"

#
"Cancel"
"Annuleren"

#MSG_SD_INSERTED c=0 r=0
"Card inserted"
"SD ingestoken"

#MSG_SD_REMOVED c=0 r=0
"Card removed"
"SD verwijderd"

#MSG_NOT_COLOR c=0 r=0
"Color not correct"
"Kleur niet juist"

#MSG_COOLDOWN c=0 r=0
"Cooldown"
"Afkoelen"

#
"Copy selected language?"
"Geselecteerde taal kopieren?"

#MSG_CRASHDETECT_ON c=0 r=0
"Crash det.   [on]"
"Crashdet.   [aan]"

#MSG_CRASHDETECT_NA c=0 r=0
"Crash det.  [N/A]"
"Crashdet.   [N/V]"

#MSG_CRASHDETECT_OFF c=0 r=0
"Crash det.  [off]"
"Crashdet.   [uit]"

#MSG_CRASH_DETECTED c=20 r=1
"Crash detected."
"Crash gedetecteerd."

#
"Crash detected. Resume print?"
"Crash gedetecteerd. Print voorzetten?"

#
"Crash"
""

#MSG_CURRENT c=19 r=1
"Current"
"Actueel"

#MSG_DATE c=17 r=1
"Date:"
"Datum:"

#MSG_DISABLE_STEPPERS c=0 r=0
"Disable steppers"
"Motoren uit"

#MSG_BABYSTEP_Z_NOT_SET c=20 r=12
"Distance between tip of the nozzle and the bed surface has not been set yet. Please follow the manual, chapter First steps, section First layer calibration."
"Afstand tussen tip van de tuit en het print oppervlak is nog niet vastgesteld. Volg de handleiding, First steps, sectie First layer calibration."

#MSG_WIZARD_REPEAT_V2_CAL c=20 r=7
"Do you want to repeat last step to readjust distance between nozzle and heatbed?"
"Wilt u de laatste stap herhalen om de afstand tussen de tuit en de bed opnieuw in te stellen?"

#MSG_EXTRUDER_CORRECTION c=9 r=0
"E-correct"
"E-correctie"

#MSG_EJECT_FILAMENT c=17 r=1
"Eject filament"
"Fil. uitwerpen"

#MSG_EJECT_FILAMENT1 c=17 r=1
"Eject filament 1"
"Fil.1 uitwerpen"

#MSG_EJECT_FILAMENT2 c=17 r=1
"Eject filament 2"
"Fil.2 uitwerpen"

#MSG_EJECT_FILAMENT3 c=17 r=1
"Eject filament 3"
"Fil.3 uitwerpen"

#MSG_EJECT_FILAMENT4 c=17 r=1
"Eject filament 4"
"Fil.4 uitwerpen"

#MSG_EJECT_FILAMENT5 c=17 r=1
"Eject filament 5"
"Fil.5 uitwerpen"

#
"Eject"
"Uitwerpen"

#MSG_EJECTING_FILAMENT c=20 r=1
"Ejecting filament"
"Werp filament uit"

#MSG_SELFTEST_ENDSTOP_NOTHIT c=20 r=1
"Endstop not hit"
"Endstop niet geraakt"

#MSG_SELFTEST_ENDSTOP c=0 r=0
"Endstop"
""

#MSG_SELFTEST_ENDSTOPS c=0 r=0
"Endstops"
""

#MSG_STACK_ERROR c=20 r=4
"Error - static memory has been overwritten"
"Fout - het statische geheugen is overschreven"

#MSG_FSENS_NOT_RESPONDING c=20 r=4
"ERROR: Filament sensor is not responding, please check connection."
"FOUT: Filamentsensor reageert niet, controleer de verbinding."

#MSG_ERROR c=0 r=0
"ERROR:"
"FOUT:"

#
"External SPI flash W25X20CL not responding."
""

#
"Extruder 1"
""

#
"Extruder 2"
""

#
"Extruder 3"
""

#
"Extruder 4"
""

#MSG_SELFTEST_EXTRUDER_FAN_SPEED c=18 r=0
"Extruder fan:"
""

#MSG_INFO_EXTRUDER c=15 r=1
"Extruder info"
""

#MSG_MOVE_E c=0 r=0
"Extruder"
""

#
"Fail stats MMU"
"MMU-Fouten"

#MSG_FSENS_AUTOLOAD_ON c=17 r=1
"F. autoload  [on]"
"F.autoladen [aan]"

#MSG_FSENS_AUTOLOAD_NA c=17 r=1
"F. autoload [N/A]"
"F.autoladen [N/V]"

#MSG_FSENS_AUTOLOAD_OFF c=17 r=1
"F. autoload [off]"
"F.autoladen [uit]"

#
"Fail stats"
"Foutstatistieken"

#MSG_FAN_SPEED c=14 r=0
"Fan speed"
"Fan snelh."

#MSG_SELFTEST_FAN c=20 r=0
"Fan test"
""

#MSG_FANS_CHECK_ON c=17 r=1
"Fans check   [on]"
"Fans check  [aan]"

#MSG_FANS_CHECK_OFF c=17 r=1
"Fans check  [off]"
"Fans check  [uit]"

#MSG_FSENSOR_ON c=0 r=0
"Fil. sensor  [on]"
"Fil. sensor [aan]"

#MSG_RESPONSE_POOR c=20 r=2
"Fil. sensor response is poor, disable it?"
"Fil. sensorreactie is slecht, uitschakelen?"

#MSG_FSENSOR_NA c=0 r=0
"Fil. sensor [N/A]"
"Fil. sensor [N/V]"

#MSG_FSENSOR_OFF c=0 r=0
"Fil. sensor [off]"
"Fil. sensor [uit]"

#
"Filam. runouts"
""

#MSG_FILAMENT_CLEAN c=20 r=2
"Filament extruding & with correct color?"
"Filament extrudeert met de juiste kleur?"

#MSG_NOT_LOADED c=19 r=0
"Filament not loaded"
"Fil. niet geladen"

#MSG_FILAMENT_SENSOR c=20 r=0
"Filament sensor"
"Filamentsensor"

#MSG_SELFTEST_FILAMENT_SENSOR c=18 r=0
"Filament sensor:"
"Filamentsensor :"

#MSG_FILAMENT_USED c=19 r=1
"Filament used"
"Gebruikte filament"

#MSG_PRINT_TIME c=19 r=1
"Print time"
"Print tijd"

#MSG_FILE_INCOMPLETE c=20 r=2
"File incomplete. Continue anyway?"
"Bestand onvolledig. Toch doorgaan?"

#MSG_FINISHING_MOVEMENTS c=20 r=1
"Finishing movements"
"Voortgang afwerken"

#MSG_V2_CALIBRATION c=17 r=1
"First layer cal."
"Eerste laag kal."

#MSG_WIZARD_SELFTEST c=20 r=8
"First, I will run the selftest to check most common assembly problems."
"Ten eerste zullen we de zelftest uitvoeren om de meest voorkomende montageproblemen te controleren."

#
"Fix the issue and then press button on MMU unit."
"Los het probleem op en druk vervolgens op de knop op de MMU-eenheid."

#MSG_FLOW c=0 r=0
"Flow"
"Stromen"

#MSG_PRUSA3D_FORUM c=0 r=0
"forum.prusa3d.com"
""

#MSG_SELFTEST_COOLING_FAN c=20 r=0
"Front print fan?"
"Voorzijde fan?"

#MSG_BED_CORRECTION_FRONT c=14 r=1
"Front side[um]"
"Voorkant  [um]"

#MSG_SELFTEST_FANS c=0 r=0
"Front/left fans"
"Fans voor/links"

#MSG_SELFTEST_HEATERTHERMISTOR c=0 r=0
"Heater/Thermistor"
"Verwarmer/Therm."

#MSG_BED_HEATING_SAFETY_DISABLED c=0 r=0
"Heating disabled by safety timer."
"Verwarming uitgeschakeld door veiligheidstimer."

#MSG_HEATING_COMPLETE c=20 r=0
"Heating done."
"Opwarmen klaar."

#MSG_HEATING c=0 r=0
"Heating"
"Opwarmen"

#MSG_WIZARD_WELCOME c=20 r=7
"Hi, I am your Original Prusa i3 printer. Would you like me to guide you through the setup process?"
"Hallo, ik ben uw originele Prusa i3 printer. Zullen we beginnen met het installatieproces?"

#MSG_WIZARD_WELCOME c=20 r=7
"Hi, I am your AnDi printer. Would you like me to guide you through the setup process?"
"Hallo, ik ben uw AnDi printer. Zullen we beginnen met het installatieproces?"

#MSG_PRUSA3D_HOWTO c=0 r=0
"howto.prusa3d.com"
""

#
"Change extruder"
"Wissel extruder"

#MSG_FILAMENTCHANGE c=0 r=0
"Change filament"
"Wissel filament"

#MSG_CHANGE_SUCCESS c=0 r=0
"Change success!"
"Wissel geslaagd!"

#MSG_CORRECTLY c=20 r=0
"Changed correctly?"
"Wissel ok?"

#MSG_SELFTEST_CHECK_BED c=20 r=0
"Checking bed     "
"Controleer bed   "

#MSG_SELFTEST_CHECK_ENDSTOPS c=20 r=0
"Checking endstops"
"Controleer endstops"

#MSG_SELFTEST_CHECK_HOTEND c=20 r=0
"Checking hotend  "
"Controleer hotend"

#MSG_SELFTEST_CHECK_FSENSOR c=20 r=0
"Checking sensors "
"Controleer sensors"

#MSG_SELFTEST_CHECK_X c=20 r=0
"Checking X axis  "
"Controleer X as  "

#MSG_SELFTEST_CHECK_Y c=20 r=0
"Checking Y axis  "
"Controleer Y as  "

#MSG_SELFTEST_CHECK_Z c=20 r=0
"Checking Z axis  "
"Controleer Z as  "

#MSG_CHOOSE_EXTRUDER c=20 r=1
"Choose extruder:"
"Kies extruder:"

#MSG_CHOOSE_FILAMENT c=20 r=1
"Choose filament:"
"Kies filament:"

#MSG_FILAMENT c=17 r=1
"Filament"
""

#MSG_WIZARD_XYZ_CAL c=20 r=8
"I will run xyz calibration now. It will take approx. 12 mins."
"Begin nu met xyz-kalibratie. Het duurt ongeveer 12 min."

#MSG_WIZARD_Z_CAL c=20 r=8
"I will run z calibration now."
"Begin nu met z-kalibratie."

#MSG_WIZARD_V2_CAL_2 c=20 r=12
"I will start to print line and you will gradually lower the nozzle by rotating the knob, until you reach optimal height. Check the pictures in our handbook in chapter Calibration."
"Begin de Kal.-lijn te printen, draai de knop tot je de optimale hoogte bereikt. Controleer de afbeeldingen in ons handboek in hoofdstuk Calibration."

#MSG_IMPROVE_BED_OFFSET_AND_SKEW_LINE1 c=60 r=0
"Improving bed calibration point"
"Nauwkeurigheid verbeteren bij kalibratiepunt"

#MSG_WATCH c=0 r=0
"Info screen"
"Info scherm"

#MSG_FILAMENT_LOADING_T0 c=20 r=4
"Insert filament into extruder 1. Click when done."
"Voer filament in extruder 1 in en druk de knop."

#MSG_FILAMENT_LOADING_T1 c=20 r=4
"Insert filament into extruder 2. Click when done."
"Voer filament in extruder 2 in en druk de knop."

#MSG_FILAMENT_LOADING_T2 c=20 r=4
"Insert filament into extruder 3. Click when done."
"Voer filament in extruder 3 in en druk de knop."

#MSG_FILAMENT_LOADING_T3 c=20 r=4
"Insert filament into extruder 4. Click when done."
"Voer filament in extruder 4 in en druk de knop."

#
"Is filament 1 loaded?"
"Is filament 1 geladen?"

#MSG_INSERT_FILAMENT c=20 r=0
"Insert filament"
"Voer filament in"

#MSG_WIZARD_FILAMENT_LOADED c=20 r=2
"Is filament loaded?"
"Is filament geladen?"

#MSG_WIZARD_PLA_FILAMENT c=20 r=2
"Is it PLA filament?"
"Is het PLA filament?"

#MSG_PLA_FILAMENT_LOADED c=20 r=2
"Is PLA filament loaded?"
"Is PLA filament geladen?"

#MSG_STEEL_SHEET_CHECK c=20 r=2
"Is steel sheet on heatbed?"
"Ligt de staalplaat op het bed?"

#MSG_FIND_BED_OFFSET_AND_SKEW_ITERATION c=20 r=0
"Iteration "
""

#
"Last print failures"
"Laatste printfouten"

#
"Last print"
"Laatste print"

#MSG_SELFTEST_EXTRUDER_FAN c=20 r=0
"Left hotend fan?"
"Linker hotend fan?"

#
"Left"
"Links"

#MSG_BED_CORRECTION_LEFT c=14 r=1
"Left side [um]"
"Linkerkant[um]"

#
"Lin. correction"
"Lineaire correctie"

#MSG_BABYSTEP_Z c=0 r=0
"Live adjust Z"
"Live Z aanpassen"

#MSG_LOAD_FILAMENT c=17 r=0
"Load filament"
"Filament laden"

#MSG_LOADING_COLOR c=0 r=0
"Loading color"
"Laden kleur"

#MSG_LOADING_FILAMENT c=20 r=0
"Loading filament"
"Laden filament"

#MSG_LOOSE_PULLEY c=20 r=1
"Loose pulley"
"Losse riemschijf"

#
"Load to nozzle"
"Tot tuit laden"

#MSG_M117_V2_CALIBRATION c=25 r=1
"M117 First layer cal."
"M117 eerste laag kal."

#MSG_MAIN c=0 r=0
"Main"
"Hoofdmenu"

#MSG_MEASURE_BED_REFERENCE_HEIGHT_LINE1 c=60 r=0
"Measuring reference height of calibration point"
"Referentie hoogte van het kalibratiepunt meten"

#MSG_MESH_BED_LEVELING c=0 r=0
"Mesh Bed Leveling"
""

#MSG_MMU_OK_RESUMING_POSITION c=20 r=4
"MMU OK. Resuming position..."
"MMU OK. Positie hervatten..."

#MSG_MMU_OK_RESUMING_TEMPERATURE c=20 r=4
"MMU OK. Resuming temperature..."
"MMU OK. Temperatuur hervatten..."

#
"Measured skew"
"Scheefheid"

#
"MMU fails"
"MMU fout"

#
"MMU load failed     "
"MMU laden mislukt   "

#
"MMU load fails"
"MMU laadfout"

#MSG_MMU_OK_RESUMING c=20 r=4
"MMU OK. Resuming..."
"MMU OK. Hervatten..."

#MSG_STEALTH_MODE_OFF c=0 r=0
"Mode     [Normal]"
"Stand   [Normaal]"

#MSG_SILENT_MODE_ON c=0 r=0
"Mode     [silent]"
"Stand      [stil]"

#
"MMU needs user attention."
"MMU heeft gebruikersaandacht nodig."

#
"MMU power fails"
"MMU stroomstor."

#MSG_STEALTH_MODE_ON c=0 r=0
"Mode    [Stealth]"
"Stand   [Stealth]"

#MSG_AUTO_MODE_ON c=0 r=0
"Mode [auto power]"
"Mode[automatisch]"

#MSG_SILENT_MODE_OFF c=0 r=0
"Mode [high power]"
"Stand      [hoog]"

#
"MMU2 connected"
"MMU2 verbonden"

#MSG_SELFTEST_MOTOR c=0 r=0
"Motor"
""

#MSG_MOVE_AXIS c=0 r=0
"Move axis"
"As verplaatsen"

#MSG_MOVE_X c=0 r=0
"Move X"
"Verplaats X"

#MSG_MOVE_Y c=0 r=0
"Move Y"
"Verplaats Y"

#MSG_MOVE_Z c=0 r=0
"Move Z"
"Verplaats Z"

#MSG_NO_MOVE c=0 r=0
"No move."
"Geen beweging."

#MSG_NO_CARD c=0 r=0
"No SD card"
"Geen SD kaart"

#
"N/A"
"N/V"

#MSG_NO c=0 r=0
"No"
"Nee"

#MSG_SELFTEST_NOTCONNECTED c=0 r=0
"Not connected"
"Niet verbonden"

#
"New firmware version available:"
"Nieuwe firmware versie beschikbaar:"

#
"No "
"Nee "

#MSG_SELFTEST_FAN_NO c=19 r=0
"Not spinning"
"Beweegt niet"

#MSG_WIZARD_V2_CAL c=20 r=8
"Now I will calibrate distance between tip of the nozzle and heatbed surface."
"Begin met kalibratie tussen de tuit en het bed."

#MSG_WIZARD_WILL_PREHEAT c=20 r=4
"Now I will preheat nozzle for PLA."
"Opwarmen van de tuit voor PLA voor."

#MSG_NOZZLE c=7 r=0
"Nozzle"
"Tuit"

#MSG_DEFAULT_SETTINGS_LOADED c=20 r=4
"Old settings found. Default PID, Esteps etc. will be set."
"Oude instellingen gevonden. Standaard PID, E-steps etc. instellingen worden geladen."

#
"Now remove the test print from steel sheet."
"Verwijder nu de testprint van staalplaat."

#
"Nozzle FAN"
"Tuit fan"

#MSG_PAUSE_PRINT c=0 r=0
"Pause print"
"Print pauzeren"

#MSG_PID_RUNNING c=20 r=1
"PID cal.           "
"PID kal.           "

#MSG_PID_FINISHED c=20 r=1
"PID cal. finished"
"PID kalibratie klaar"

#MSG_PID_EXTRUDER c=17 r=1
"PID calibration"
"PID kalibratie"

#MSG_PINDA_PREHEAT c=20 r=1
"PINDA Heating"
"PINDA opwarmen"

#MSG_PAPER c=20 r=8
"Place a sheet of paper under the nozzle during the calibration of first 4 points. If the nozzle catches the paper, power off the printer immediately."
"Leg een vel papier onder de tuit tijdens de kalibratie van de eerste 4 punten. Als de tuit het papier beweegt, de printer onmiddellijk uitschakelen."

#MSG_WIZARD_CLEAN_HEATBED c=20 r=8
"Please clean heatbed and then press the knob."
"Maak het bed schoon en druk op de knop."

#MSG_CONFIRM_NOZZLE_CLEAN c=20 r=8
"Please clean the nozzle for calibration. Click when done."
"Reinig de tuit voor de kalibratie. Druk op de knop wanneer gereed."

#MSG_SELFTEST_PLEASECHECK c=0 r=0
"Please check :"
"Controleer aub:"

#MSG_WIZARD_CALIBRATION_FAILED c=20 r=8
"Please check our handbook and fix the problem. Then resume the Wizard by rebooting the printer."
"Controleer aub ons handboek en los het probleem op. Hervat vervolgens de wizard door de printer opnieuw te starten."

#MSG_WIZARD_LOAD_FILAMENT c=20 r=8
"Please insert PLA filament to the extruder, then press knob to load it."
"Voer PLA filament in de extruder, druk dan op knop om het te laden."

#MSG_PLEASE_LOAD_PLA c=20 r=4
"Please load PLA filament first."
"Voer eerst PLA filament aub."

#MSG_CHECK_IDLER c=20 r=4
"Please open idler and remove filament manually."
"Open rondsel en verwijder filament handmatig."

#MSG_PLACE_STEEL_SHEET c=20 r=4
"Please place steel sheet on heatbed."
"Leg staalplaat op bed."

#MSG_PRESS_TO_UNLOAD c=20 r=4
"Please press the knob to unload filament"
"Druk op de knop om filament te verwijderen"

#
"Please insert PLA filament to the first tube of MMU, then press the knob to load it."
"Voer PLA-filament in de eerste buis van de MMU en druk op de knop om deze te laden."

#MSG_PULL_OUT_FILAMENT c=20 r=4
"Please pull out filament immediately"
"Trek onmiddellijk de filament eruit"

#MSG_EJECT_REMOVE c=20 r=4
"Please remove filament and then press the knob."
"Trek onmiddellijk filament eruit en druk vervolgens op de knop."

#MSG_REMOVE_STEEL_SHEET c=20 r=4
"Please remove steel sheet from heatbed."
"Verwijder staalplaat van het bed."

#MSG_RUN_XYZ c=20 r=4
"Please run XYZ calibration first."
"Voer eerst de XYZ-kalibratie uit."

#MSG_UPDATE_MMU2_FW c=20 r=4
"Please update firmware in your MMU2. Waiting for reset."
"Gelieve de firmware te vernieuwen in uw MMU2. Wacht op reset."

#MSG_PLEASE_WAIT c=20 r=0
"Please wait"
"Even geduld aub"

#
"Please remove shipping helpers first."
"Verwijder eerst de transport beschermers."

#MSG_PREHEAT_NOZZLE c=20 r=0
"Preheat the nozzle!"
"Tuit voorverwarmen!"

#MSG_PREHEAT c=0 r=0
"Preheat"
"Voorverwarmen"

#MSG_WIZARD_HEATING c=20 r=3
"Preheating nozzle. Please wait."
"Opwarmen van de tuit. Wacht aub."

#
"Please upgrade."
"Voer een upgrade uit."

#MSG_PRESS_TO_PREHEAT c=20 r=4
"Press knob to preheat nozzle and continue."
"Druk op de knop om de tuit voor te verwarmen en door te gaan."

#
"Power failures"
"Stroomstoringen"

#MSG_PRINT_ABORTED c=20 r=0
"Print aborted"
"Print afgebroken"

#
"Preheating to load"
"Opwarmen invoeren"

#
"Preheating to unload"
"Opwarmen uitwerpen"

#MSG_SELFTEST_PRINT_FAN_SPEED c=18 r=0
"Print fan:"
""

#MSG_CARD_MENU c=0 r=0
"Print from SD"
"Print van SD"

#
"Press the knob"
"Druk op knop"

#MSG_PRINT_PAUSED c=20 r=1
"Print paused"
"Print pauzeren"

#
"Press the knob to resume nozzle temperature."
"Druk op de knop om de temperatuur van de tuit te hervatten."

#MSG_FOLLOW_CALIBRATION_FLOW c=20 r=8
"Printer has not been calibrated yet. Please follow the manual, chapter First steps, section Calibration flow."
"Printer is nog niet gekalibreerd. Volg de handleiding, hoofdstuk First steps, sectie Calibration flow."

#
"Print FAN"
"Print fan"

#WELCOME_MSG c=20 r=0
"Prusa i3 MK2.5"
"\x00"

#WELCOME_MSG c=20 r=0
"Prusa i3 MK3"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK2.5-320"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK2.5-220"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK3-320"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK3-220"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK2.5-420"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK3-420"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK3S-220"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK3S-320"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK3S-420"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK3-BMG-220"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK3-BMG-320"
"\x00"

#WELCOME_MSG c=20 r=0
"AnDi MK3-BMG-420"
"\x00"

#MSG_PRUSA3D c=0 r=0
"prusa3d.com"
"\x00"

#MSG_BED_CORRECTION_REAR c=14 r=1
"Rear side [um]"
"Achterkant[um]"

#MSG_RECOVERING_PRINT c=20 r=1
"Recovering print    "
"Print herstellen    "

#MSG_REMOVE_OLD_FILAMENT c=20 r=4
"Remove old filament and press the knob to start loading new filament."
"Verwijder de oude filament en druk op de knop om nieuwe filament te laden."

#
"Prusa i3 MK3S OK."
""

#
"Prusa i3 MK2 ready."
"Prusa i3 MK2 klaar."

#MSG_CALIBRATE_BED_RESET c=0 r=0
"Reset XYZ calibr."
"Reset XYZ kalibr."

#MSG_BED_CORRECTION_RESET c=0 r=0
"Reset"
""

#MSG_RESUME_PRINT c=0 r=0
"Resume print"
"Print hervatten"

#MSG_RESUMING_PRINT c=20 r=1
"Resuming print"
"Hervatten print"

#MSG_BED_CORRECTION_RIGHT c=14 r=1
"Right side[um]"
"Recht.kant[um]"

#MSG_SECOND_SERIAL_ON c=17 r=1
"RPi port     [on]"
"RPi port    [aan]"

#MSG_SECOND_SERIAL_OFF c=17 r=1
"RPi port    [off]"
"RPi port    [uit]"

#MSG_WIZARD_RERUN c=20 r=7
"Running Wizard will delete current calibration results and start from the beginning. Continue?"
"Starten van de Wizard verwijdert de huidige kalibreringsresultaten en begint vanaf het begin. Doorgaan?"

#MSG_TOSHIBA_FLASH_AIR_COMPATIBILITY_OFF c=19 r=1
"SD card  [normal]"
"SD card [normaal]"

#MSG_TOSHIBA_FLASH_AIR_COMPATIBILITY_ON c=19 r=1
"SD card [flshAir]"
""

#
"Right"
"Rechts"

#MSG_FIND_BED_OFFSET_AND_SKEW_LINE1 c=60 r=0
"Searching bed calibration point"
"Zoeken bed kalibratiepunt"

#MSG_LANGUAGE_SELECT c=0 r=0
"Select language"
"Kies taal"

#MSG_SELFTEST_OK c=0 r=0
"Self test OK"
"Zelftest  OK"

#MSG_SELFTEST_START c=20 r=0
"Self test start  "
"Zelftest strat   "

#MSG_SELFTEST c=0 r=0
"Selftest         "
"Zelftest         "

#MSG_SELFTEST_ERROR c=0 r=0
"Selftest error !"
"Zelftest fout  !"

#MSG_SELFTEST_FAILED c=20 r=0
"Selftest failed  "
"Zelftest mislukt "

#MSG_FORCE_SELFTEST c=20 r=8
"Selftest will be run to calibrate accurate sensorless rehoming."
"Zelftest zal worden uitgevoerd om nauwkeurige sensorloze auto positie te kalibreren."

#
"Select nozzle preheat temperature which matches your material."
"Selecteer de voorverwarmingstemperatuur van de tuit die overeenkomt met uw materiaal."

#
"Select PLA filament:"
"Selecteer PLA-filament:"

#MSG_SET_TEMPERATURE c=19 r=1
"Set temperature:"
"Temp. instellen:"

#MSG_SETTINGS c=0 r=0
"Settings"
"Instellingen"

#MSG_SHOW_END_STOPS c=17 r=1
"Show end stops"
"Toon endstops"

#
"Sensor state"
"Sensorstatus"

#
"Sensors info"
"Sensoren info"

#
"Show pinda state"
"Toon PINDA status"

#MSG_FILE_CNT c=20 r=4
"Some files will not be sorted. Max. No. of files in 1 folder for sorting is 100."
"Sommige bestanden worden niet gesorteerd omdat het maximum aantal bestanden per map 100 is."

#MSG_SORT_NONE c=17 r=1
"Sort       [none]"
"Sortering  [geen]"

#MSG_SORT_TIME c=17 r=1
"Sort       [time]"
"Sortering  [tijd]"

#
"Severe skew"
"Erg scheef"

#MSG_SORT_ALPHA c=17 r=1
"Sort   [alphabet]"
"Sort    [alfabet]"

#MSG_SORTING c=20 r=1
"Sorting files"
"Bestanden sorteren"

#MSG_SOUND_LOUD c=17 r=1
"Sound      [loud]"
"Geluid     [hard]"

#
"Slight skew"
"Iets scheef"

#MSG_SOUND_MUTE c=17 r=1
"Sound      [mute]"
"Geluid      [uit]"

#
"Some problem encountered, Z-leveling enforced ..."
"Er is een probleem opgetreden, Z-kalibratie afgedwongen ..."

#MSG_SOUND_ONCE c=17 r=1
"Sound      [once]"
"Geluid  [eenmaal]"

#MSG_SOUND_SILENT c=17 r=1
"Sound    [silent]"
"Geluid     [stil]"

#MSG_SPEED c=0 r=0
"Speed"
"Snelheid"

#MSG_SELFTEST_FAN_YES c=19 r=0
"Spinning"
"Draait"

#MSG_TEMP_CAL_WARNING c=20 r=4
"Stable ambient temperature 21-26C is needed a rigid stand is required."
"Stabiele omgevingstemperatuur van 21-26C is nodig, een stevige stand is vereist."

#MSG_STATISTICS c=0 r=0
"Statistics  "
"Statistieken"

#MSG_STOP_PRINT c=0 r=0
"Stop print"
""

#MSG_STOPPED c=0 r=0
"STOPPED. "
"GESTOPT. "

#MSG_SUPPORT c=0 r=0
"Support"
""

#MSG_SELFTEST_SWAPPED c=0 r=0
"Swapped"
"Gewisseld"

#MSG_TEMP_CALIBRATION c=20 r=1
"Temp. cal.          "
"Tempkalibratie      "

#MSG_TEMP_CALIBRATION_ON c=20 r=1
"Temp. cal.   [on]"
"Tempkal.    [aan]"

#MSG_TEMP_CALIBRATION_OFF c=20 r=1
"Temp. cal.  [off]"
"Tempkal.    [uit]"

#MSG_CALIBRATION_PINDA_MENU c=17 r=1
"Temp. calibration"
"Tempkalibratie"

#MSG_TEMP_CAL_FAILED c=20 r=8
"Temperature calibration failed"
"Temperatuurkalibratie mislukt"

#MSG_TEMP_CALIBRATION_DONE c=20 r=12
"Temperature calibration is finished and active. Temp. calibration can be disabled in menu Settings->Temp. cal."
"Temperatuurkalibratie kan worden uitgeschakeld in het menu Instellingen-> Tempkalibratie."

#MSG_TEMPERATURE c=0 r=0
"Temperature"
"Temperatuur"

#MSG_MENU_TEMPERATURES c=15 r=1
"Temperatures"
"Temperaturen"

#MSG_FOLLOW_Z_CALIBRATION_FLOW c=20 r=4
"There is still a need to make Z calibration. Please follow the manual, chapter First steps, section Calibration flow."
"Er is nog steeds een noodzaak om de Z-kalibratie uit te voeren. Volg de handleiding, hoofdstuk First steps, section Calibration flow."

#
"Total filament"
"Totaal fil.   "

#
"Total print time"
"Totaal printtijd"

#MSG_TUNE c=0 r=0
"Tune"
"Fijnafstemming"

#
"Unload"
"Verwijderen"

#
"Unload all"
"Alles verwijderen"

#
"Total failures"
"Totaal fouten"

#
"to load filament"
"om filament te laden"

#
"to unload filament"
"om filament te laden"

#MSG_UNLOAD_FILAMENT c=17 r=0
"Unload filament"
"Filament uitwerpen"

#MSG_UNLOADING_FILAMENT c=20 r=1
"Unloading filament"
"Uitwerpen filament"

#
"Total"
"Totaal"

#MSG_USED c=19 r=1
"Used during print"
"Gebruikt bij print"

#MSG_MENU_VOLTAGES c=15 r=1
"Voltages"
"Spanning"

#
"unknown"
"onbekend"

#MSG_USERWAIT c=0 r=0
"Wait for user..."
"Wacht op gebruiker ..."

#MSG_WAITING_TEMP c=20 r=3
"Waiting for nozzle and bed cooling"
"Wachten op afkoelen van tuit en bed"

#MSG_WAITING_TEMP_PINDA c=20 r=3
"Waiting for PINDA probe cooling"
"Wachten op afkoelen van PINDA"

#
"Use unload to remove filament 1 if it protrudes outside of the rear MMU tube. Use eject if it is hidden in tube."
"Gebruik ontladen om filament 1 te verwijderen als het uitsteekt buiten de achterste MMU-buis. Gebruik uitwerpen als deze in de tube is verborgen."

#MSG_CHANGED_BOTH c=20 r=4
"Warning: both printer type and motherboard type changed."
"Waarschuwing: zowel het printertype als het moederbordtype is gewijzigd."

#MSG_CHANGED_MOTHERBOARD c=20 r=4
"Warning: motherboard type changed."
"Waarschuwing: type moederbord gewijzigd."

#MSG_CHANGED_PRINTER c=20 r=4
"Warning: printer type changed."
"Waarschuwing: printertype gewijzigd."

#MSG_UNLOAD_SUCCESSFUL c=20 r=2
"Was filament unload successful?"
"Is filament succesvol verwijderd?"

#MSG_SELFTEST_WIRINGERROR c=0 r=0
"Wiring error"
"Aansluitingsfout"

#MSG_WIZARD c=17 r=1
"Wizard"
""

#MSG_XYZ_DETAILS c=19 r=1
"XYZ cal. details"
"XZY kal. details"

#MSG_BED_SKEW_OFFSET_DETECTION_FITTING_FAILED c=20 r=8
"XYZ calibration failed. Please consult the manual."
"XYZ-kalibratie mislukt. Raadpleeg de handleiding aub."

#MSG_YES c=0 r=0
"Yes"
"Ja"

#MSG_WIZARD_QUIT c=20 r=8
"You can always resume the Wizard from Calibration -> Wizard."
"U kunt de wizard altijd hervatten via Kalibratie -> Wizard."

#MSG_BED_SKEW_OFFSET_DETECTION_SKEW_EXTREME c=20 r=8
"XYZ calibration all right. Skew will be corrected automatically."
"XYZ-kalibratie in orde. Scheefheid zal automatisch worden gecorrigeerd."

#MSG_BED_SKEW_OFFSET_DETECTION_SKEW_MILD c=20 r=8
"XYZ calibration all right. X/Y axes are slightly skewed. Good job!"
"XYZ-kalibratie in orde. X / Y-assen zijn licht scheef. Goed gedaan!"

#
"X-correct"
"X-correctie"

#MSG_BED_SKEW_OFFSET_DETECTION_PERFECT c=20 r=8
"XYZ calibration ok. X/Y axes are perpendicular. Congratulations!"
"XYZ-kalibratie ok. X / Y-assen staan loodrecht. Gefeliciteerd!"

#MSG_BED_SKEW_OFFSET_DETECTION_WARNING_FRONT_BOTH_FAR c=20 r=8
"XYZ calibration compromised. Front calibration points not reachable."
"XYZ-kalibratie niet gelukt. Voorste kalibratiepunten niet bereikbaar."

#MSG_BED_SKEW_OFFSET_DETECTION_WARNING_FRONT_RIGHT_FAR c=20 r=8
"XYZ calibration compromised. Right front calibration point not reachable."
"XYZ-kalibratie niet gelukt. Rechter voor kalibratiepunt niet bereikbaar."

#MSG_BED_SKEW_OFFSET_DETECTION_WARNING_FRONT_LEFT_FAR c=20 r=8
"XYZ calibration compromised. Left front calibration point not reachable."
"XYZ-kalibratie niet gelukt. Linker voor kalibratiepunt niet bereikbaar."

#MSG_LOAD_ALL c=17 r=0
"Load all"
"Laad alle"

#MSG_LOAD_FILAMENT_1 c=17 r=0
"Load filament 1"
"Filament 1 laden"

#
"XYZ calibration failed. Bed calibration point was not found."
"XYZ-kalibratie mislukt. Bed ijkpunt niet gevonden."

#
"XYZ calibration failed. Front calibration points not reachable."
"XYZ-kalibratie mislukt. Voorste kalibratiepunten niet bereikbaar."

#
"XYZ calibration failed. Left front calibration point not reachable."
"XYZ-kalibratie mislukt. Linker voor kalibratiepunt niet bereikbaar."

#MSG_LOAD_FILAMENT_2 c=17 r=0
"Load filament 2"
"Filament 2 laden"

#
"XYZ calibration failed. Right front calibration point not reachable."
"XYZ-kalibratie mislukt. Rechter voor kalibratiepunt niet bereikbaar."

#MSG_LOAD_FILAMENT_3 c=17 r=0
"Load filament 3"
"Filament 3 laden"

#
"Y distance from min"
"Y afstand van min"

#
"Y-correct"
"Y-correctie"

#MSG_LOAD_FILAMENT_4 c=17 r=0
"Load filament 4"
"Filament 4 laden"

#MSG_LOAD_FILAMENT_5 c=17 r=0
"Load filament 5"
"Filament 5 laden"

#MSG_OFF c=0 r=0
" [off]"
" [uit]"
